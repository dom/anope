anope (2.0.15-1) unstable; urgency=medium

  * New upstream release (Closes: #1063653)
    - Several fixes and improvements including a security-sensitive
      fix for being able to reset the password of suspended accounts.
      (Closes: #1067571)
    - update default configs from examples
    - update debian/copyright
  * Install upstream changes in /usr/share/doc
  * Fix lintian error depends-on-obsolete-package Depends: lsb-base
    (>= 3.2-14)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 23 Mar 2024 21:14:00 +0000

anope (2.0.12-1) unstable; urgency=medium

  * New upstream release
    - updated default configs from examples
    - update debian/copyright
  * Switch to pcre2 (Closes: #1000019)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 03 Jan 2023 18:04:10 +0000

anope (2.0.9-1) unstable; urgency=medium

  * New upstream release
    - updated default configs from examples
    - update debian/copyright
  * Add Breaks on ircd-hybrid << 8.2.23 due to incompatible protocol
    change
  * Add Rules-Requires-Root declaration (thanks, Lintian)
  * Update debhelper compat to 13

 -- Dominic Hargreaves <dom@earth.li>  Sun, 25 Oct 2020 19:42:47 +0000

anope (2.0.7-1) unstable; urgency=medium

  * New upstream release
    - updated default configs from examples
    - update debian/copyright
  * Remove discouraged pattern of disabling startup in /etc/default
    (fixes Lintian error)
  * Tweak Linitian override for non-standard file modes
  * Install apparmor profile. Thanks to LaMont Jones for providing the
    initial implementation (Closes: #785754)
  * Install systemd service. Thanks to Andreas Henriksson for providing
    the initial implementation (Closes: #950241)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 09 Aug 2020 15:52:17 +0100

anope (2.0.6-1) unstable; urgency=medium

  * New upstream release
    - update default configs from examples
  * Update Standards-Version (no changes)
  * Update debhelper compat to 10
  * Update Vcs-* fields to salsa

 -- Dominic Hargreaves <dom@earth.li>  Fri, 06 Apr 2018 22:14:58 +0100

anope (2.0.4-2) unstable; urgency=high

  * Correct Recommends typo tranport -> transport to stop Exim taking
    over from already-installed MTAs (Closes: #864668)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 13 Jun 2017 23:06:32 +0100

anope (2.0.4-1) unstable; urgency=medium

  * New upstream release
  * Switch to MySQL metapackage
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 23 Nov 2016 23:41:52 +0000

anope (2.0.3-3) unstable; urgency=medium

  * Create /var/lib/anope/db/backups to avoid anope shutting down
    when creating backups (Closes: #822371)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 24 Apr 2016 18:03:48 +0100

anope (2.0.3-2) unstable; urgency=medium

  * Don't include build date/time in compiled binary, fixing build
    reproducibility (Closes: #820152)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 23 Apr 2016 11:53:20 +0100

anope (2.0.3-1) unstable; urgency=medium

  * New upstream release (Closes: #813664)
  * Update default configuration files from upstream examples
  * Disable the webcpanel module because it results in Lintian errors
    about privacy breaches

 -- Dominic Hargreaves <dom@earth.li>  Mon, 15 Feb 2016 22:12:05 +0000

anope (2.0.2-2) unstable; urgency=medium

  * Remove reference to obsolete services in package description
  * Enable compilation of extra modules (Closes: #783184)
  * Correct location of log file in shipped config file (Closes: #783185)
  * Emit warning when the init script 'reload' function is called and
    the daemon is not running (Closes: #783183)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 27 May 2015 10:42:18 +0100

anope (2.0.2-1) unstable; urgency=medium

  * Initial release (Closes: #661341)

 -- Dominic Hargreaves <dom@earth.li>  Mon, 13 Apr 2015 00:39:32 +0100
